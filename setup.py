#!/usr/bin/env python
# coding: utf-8

from setuptools import setup, find_packages
from  find_pybind11_extensions import find_extensions, CMakeBuild
import os

packages=find_extensions()
print( packages)


def setup_package():
    doc_requires = [
        'sphinx',
        'cloud_sptheme',
        'myst-parser',
        'nbsphinx',
    ]
    setup(
        name='aspect_phase',
        author='Alessandro Mirone',
#        version=version,
        author_email = "mirone@esrf.fr",
        maintainer = "Alessandro Mirone",
        maintainer_email = "mirone@esrf.fr",

        packages=find_packages(),
        package_data = {
            'aspect_phase': [
                'cxx/buils/lib*so',
            ],
        },
        include_package_data=True,

        install_requires = [
            'psutil',
            'pytest',
            'numpy > 1.9.0',
            'pycuda',
        ],
        ext_modules=find_extensions(),
        cmdclass={"build_ext": CMakeBuild},
        
        description = "aspect phase retrieval",
        entry_points = {
            'console_scripts': [
                "aspect-create-target=aspect_phase.create_target:main",
                "aspect-z-stage-nx=aspect_phase.z_stage_nx:main",
                "deconvolve-z-stage-nx=aspect_phase.deconvolve_z_stage_nx:main",
                "pag-z-stage-nx=aspect_phase.pag_z_stage_nx:main",
                "aspect-beam-energy-profiler=aspect_phase.beam_energy_profiler:main"
            ],
        },
        zip_safe=True
    )


if __name__ == "__main__":
    setup_package()
