import xrt.backends.raycing.materials as mats
import numpy as np
import pandas as pd
import argparse
import matplotlib
matplotlib.use("agg")
from matplotlib import pyplot as plt
from numpy.polynomial import Polynomial

"""
 Bones : 70% mineral fraction 30% collagen
       mineral fraction: 85% hydroxyapatite  15% Calcium carbonate
"""


class Spectra:
    def __init__(
        self,
        excel_file_name="dose_estimator_BM05-BM18_2023.xlsx",
        intervals_kev=[40.0, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 110, 120, 130, 150, 180, 220, 260],
        n_spectra=None,
        display=False,
        img_prefix=None,
    ):
        if excel_file_name is not None:
                ##################################################################
            capital_letters = list(map(chr, range(65, 91)))
            column_names = []
            for prefix in ["", "A", "B", "C", "D"]:
                column_names = column_names + [prefix + c for c in capital_letters]
                cname_to_cnum = dict(zip(column_names, range(len(column_names))))
                sheets = pd.read_excel(excel_file_name, sheet_name=None)
                spectra_sheet = sheets["spectra"]
    
            X = spectra_sheet[spectra_sheet.columns[cname_to_cnum["A"]]][5:].to_numpy(dtype=np.float32)
            Y = spectra_sheet[spectra_sheet.columns[cname_to_cnum["AO"]]][5:].to_numpy(dtype=np.float32)
    
            Y[:] = Y / Y.sum()
    
            if n_spectra is not None:
                intervals_kev = [X[0] / 1.0e3]
                for i in range(0, n_spectra):
                    sum = 0
                    for k, val in enumerate(Y):
                        sum += val
                        if sum > (i + 1.0) / n_spectra:
                            intervals_kev.append(X[k] / 1.0e3)
    
                            break
                    else:
                        intervals_kev.append(X[-1] / 1.0e3)
                intervals_values = np.interp(np.array(intervals_kev), X / 1.0e3, Y)
    
            positions = np.searchsorted(X / 1.0e3, intervals_kev)
            total_weights = []
    
            baricenters = []
            interval_lenght = []
            for start, end in zip(positions[:-1], positions[1:]):
                my_x = np.array(X[start:end])
                my_y = np.array(Y[start:end])
    
                tw = my_y.sum()
                bari = (my_x * my_y).sum() / tw
    
                total_weights.append(tw)
                interval_lenght.append(len(my_x))
                baricenters.append(bari)
    
            baricenters = np.array(baricenters)
    
            if display or img_prefix:
                plt.plot(X / 1.0e3, Y)
                plt.plot(intervals_kev, intervals_values, "ro")
                plt.plot(baricenters / 1.0e3, total_weights / np.array(interval_lenght), "go")
                if img_prefix:
                    plt.savefig(img_prefix + "_spectra_and_intervals.png")
                if display:
                    plt.show()
    
            self.energies_kev = baricenters / 1.0e3
            self.spectral_fractions = np.array(total_weights) / np.array(total_weights).sum()
        else:
            assert (len(intervals_kev)==2 ) ," Without beam profile file one signle interval should be considered"
            
            self.spectral_fractions= np.array([1.0])
            self.energies_kev= np.array([np.array(intervals_kev).mean()])
            
class Materials:
    def get_hydroxyapatite():
        """Ca5(PO4)3(OH)"""
        mat = mats.Material(
            elements=["Ca", "O", "H", "P"],
            quantities=[5, 10, 3, 3],
            kind="auto",
            rho=3.16,
            t=None,
            table="Chantler total",  # not just photo-electric
            efficiency=None,
            efficiencyFile=None,
            name="",
        )
        return mat

    def get_quartz():
        """SiO2"""
        mat = mats.Material(
            elements=[
                "Si",
                "O",
            ],
            quantities=[1, 2],
            kind="auto",
            rho=2.65,
            t=None,
            table="Chantler total",  # not just photo-electric
            efficiency=None,
            efficiencyFile=None,
            name="",
        )
        return mat

    def get_calcite():
        """Ca5(PO4)3(OH)"""
        mat = mats.Material(
            elements=["Ca", "C", "O"],
            quantities=[1, 1, 3],
            kind="auto",
            rho=2.711,
            t=None,
            table="Chantler total",  # not just photo-electric
            efficiency=None,
            efficiencyFile=None,
            name="",
        )
        return mat

    def get_collagen():
        """C5H9O2N"""
        mat = mats.Material(
            elements=["C", "H", "O", "N"],
            quantities=[5, 9, 2, 1],
            kind="auto",
            rho=1.3,
            t=None,
            table="Chantler total",  # not just photo-electric
            efficiency=None,
            efficiencyFile=None,
            name="",
        )
        return mat

    def get_water():
        """H2O"""
        mat = mats.Material(
            elements=["O", "H"],
            quantities=[1, 2],
            kind="auto",
            rho=1.0,
            t=None,
            table="Chantler total",  # not just photo-electric
            efficiency=None,
            efficiencyFile=None,
            name="",
        )
        return mat

    def get_ethanol():
        """C2OH6"""
        mat = mats.Material(
            elements=["C", "O", "H"],
            quantities=[2, 1, 6],
            kind="auto",
            rho=0.789,
            t=None,
            table="Chantler total",  # not just photo-electric
            efficiency=None,
            efficiencyFile=None,
            name="",
        )
        return mat

    def mix_by_weights_percentages(mat_list, weight_percentage_list):
        weight_percentage_list = np.array(weight_percentage_list)
        rho_list = np.array([m.rho for m in mat_list])

        v_per_unit_weight_list = weight_percentage_list / rho_list

        mixture_density = 1.0 / (v_per_unit_weight_list.sum())

        # volumic_percentages = v_per_unit_weight_list / ( v_per_unit_weight_list.sum()  )

        dizio_occupancies = {}
        for m in mat_list:
            for element in m.elements:
                dizio_occupancies[element.name] = 0.0

        for w, m in zip(weight_percentage_list, mat_list):
            for element, occupancy in zip(m.elements, m.quantities):
                dizio_occupancies[element.name] += w / m.mass * occupancy

        print(list(dizio_occupancies.items()))

        elements, quantities = zip(*dizio_occupancies.items())

        mat = mats.Material(
            elements=elements,
            quantities=quantities,
            kind="auto",
            rho=mixture_density,
            t=None,
            table="Chantler total",  # not just photo-electric
            efficiency=None,
            efficiencyFile=None,
            name="",
        )
        return mat

    hydroxyapatite = get_hydroxyapatite()

    calcite = get_calcite()

    collagen = get_collagen()

    water = get_water()

    ethanol = get_ethanol()

    quartz = get_quartz()


class IndexProvider:
    def __init__(
        self,
        dense_mat,
        light_mat,
        spectra_obj,
    ):
        self.light_mat = light_mat
        self.dense_mat = dense_mat

        self.energies_kev = spectra_obj.energies_kev
        self.spectral_fractions = spectra_obj.spectral_fractions

        self.E_nominal = (self.energies_kev * self.spectral_fractions).sum()
        E_max = self.energies_kev.max()
        E_min = self.energies_kev.min()

    def get_data_for_aspect(self):
        E = self.E_nominal
        beta_n, delta_n, fraction = self.get_beta(E), self.get_delta(E), 1

        stack = []
        for E, f in zip(self.energies_kev, self.spectral_fractions):
            beta, delta, fraction = self.get_beta(E), self.get_delta(E), f
            stack.append([E / self.E_nominal, beta / beta_n, delta / delta_n, fraction])
        return self.E_nominal, delta_n / beta_n, np.array(stack)

    def get_refractive_index(self, E_kev):
        n = 1 + (
            self.dense_mat.get_refractive_index(E_kev * 1.0e3) - self.light_mat.get_refractive_index(E_kev * 1.0e3)
        )
        return n

    def get_delta(self, E_kev):
        return 1 - self.get_refractive_index(E_kev).real

    def get_beta(self, E_kev):
        return -self.get_refractive_index(E_kev).imag

    def deltabetaratio(self, E_kev):
        return self.get_delta(E_kev) / self.get_beta(E_kev)


class SpectraSegmenter:
    def __init__(self, dense_mat, light_mat, spectra_obj):
        self.light_mat = light_mat
        self.dense_mat = dense_mat

        self.energies_kev = spectra_obj.energies_kev
        self.spectral_fractions = spectra_obj.spectral_fractions

        self.E_nominal = (self.energies_kev * self.spectral_fractions).sum()

    def write_for_aspect(self, target_file):
        E = self.E_nominal
        beta_n, delta_n, fraction = self.get_beta(E), self.get_delta(E), 1

        # print(f" NOMINAL DELTA/BETA    {delta_n/beta_n:10.15e} NOMINAL E {E} "    )
        # print(" =============== NOMINAL =========================== ")
        # print(  f" {E/self.E_nominal:10.15e}    {beta_n/beta_n:10.15e}  {delta_n/delta_n:10.15e}    {fraction:10.15e}  {beta_n:10.15e} {delta_n:10.15e} " )
        # print(" =============== =========================== ")
        # print(  f""" {"E/E_nominal":25s}   {"beta/beta_n":25s}  {"delta/delta_n":25s}    {"fraction":25s}  {"beta_n":25s} {"delta_n":25s} """)

        with open(target_file, "w") as fout:
            s = f"""# E_nominal(KeV) = {self.E_nominal:10.15e}     \n"""
            s += f"""# dbr_nominal = {(delta_n/beta_n):10.15e}   \n"""
            s += f"""# {"E/E_nominal":25s}   {"beta/beta_n":25s}  {"delta/delta_n":25s}    {"fraction":25s} \n"""
            for E, f in zip(self.energies_kev, self.spectral_fractions):
                beta, delta, fraction = self.get_beta(E), self.get_delta(E), f
                print(
                    f" {E/self.E_nominal:10.15e}    {beta/beta_n:10.15e}  {delta /delta_n:10.15e}    {fraction:10.15e}  {beta:10.15e} {delta:10.15e} "
                )
                s += f"  {E/self.E_nominal:10.15e}  {beta/beta_n:10.15e}  {delta /delta_n:10.15e}  {fraction:10.15e} \n"
            fout.write(s)

    def get_refractive_index(self, E_kev):
        n = 1 + (
            self.dense_mat.get_refractive_index(E_kev * 1.0e3) - self.light_mat.get_refractive_index(E_kev * 1.0e3)
        )
        return n

    def get_delta(self, E_kev):
        return 1 - self.get_refractive_index(E_kev).real

    def get_beta(self, E_kev):
        return -self.get_refractive_index(E_kev).imag

    def deltabetaratio(self, E_kev):
        return self.get_delta(E_kev) / self.get_beta(E_kev)

    def transmission_to_um_mono_energy(self, transmission, E_kev):
        beta = self.get_beta(E_kev)

        # K0 in um**-1
        K0 = 2 * np.pi * E_kev / 12.39842 * 1.0e4

        thick_um = -np.log(transmission) / (2 * beta * K0)
        return thick_um

    def um_to_transmission_mono_energy(self, thick_um, E_kev):
        beta = self.get_beta(E_kev)

        # K0 in um**-1
        K0 = 2 * np.pi * E_kev / 12.39842 * 1.0e4

        transmission = np.exp(-2 * beta * K0 * thick_um)
        return transmission


def get_arguments():
    choices_hard = "bone", "sand"
    choices_soft = "ethanol", "water"
    parser = argparse.ArgumentParser(description="foo")
    parser.add_argument("--beam_file", required=False, help="The excel file where the beam has been calculated ")
    parser.add_argument(
        "--target_file", required=True, help="The file where the energies, delta/beta etc. etc. are written"
    )
    parser.add_argument("--hard_matter", choices=choices_hard, required=True, help=f"one of {choices_hard}")
    parser.add_argument("--soft_matter", choices=choices_soft, required=True, help=f"one of {choices_soft}")
    # parser.add_argument("--start_kev", required=True, help='the first energy of the interval' )
    # parser.add_argument("--end_kev", required=True, help='the last energy of the interval' )
    parser.add_argument("--n_points", type=int, required=False, help="the number of interval")
    parser.add_argument("--img_prefix", required=False, help="The prefix for plot images")
    parser.add_argument(
        "--display", default="0", choices=["0", "1"], required=False, help="wether display or not the plots"
    )
    parser.add_argument(
        "--intervals",
        required=False,
        type=float,
        nargs="*",
        help="Optional, if given two or more values delimiting the energy intervals ",
    )
    args = parser.parse_args()

    if args.intervals is None:
        args.intervals = []
    return args


def main():
    args = get_arguments()

    assert (args.n_points or len(args.intervals) >= 2) and (
        not (args.n_points and len(args.intervals))
    ), f"Either you specify the number of points or you give intervals"
    if len(args.intervals) == 0:
        args.intervals = None

    bone = Materials.mix_by_weights_percentages(
        [Materials.collagen, Materials.hydroxyapatite, Materials.calcite], [0.3, 0.7 * 0.86, 0.7 * 0.14]
    )

    components = [
        {"bone": bone, "ethanol": Materials.ethanol, "sand": Materials.quartz, "water": Materials.water}[what]
        for what in (args.hard_matter, args.soft_matter)
    ]

    hard, soft = components

    ##################################################################

    display = args.display == "1"

    beam_spectra = Spectra(
        excel_file_name=args.beam_file,
        n_spectra=args.n_points,
        intervals_kev=args.intervals,
        display=display,
        img_prefix=args.img_prefix,
    )

    spectra_segmenter = SpectraSegmenter(bone, Materials.ethanol, beam_spectra)

    spectra_segmenter.write_for_aspect(args.target_file)
