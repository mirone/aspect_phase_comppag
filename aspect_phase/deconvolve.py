import numpy as np
import h5py
# import fabio

from multiprocessing import Pool
import multiprocessing

import pyfftw

pyfftw.config.NUM_THREADS = multiprocessing.cpu_count()
pyfftw.interfaces.cache.enable()

pyfftw.interfaces.cache.set_keepalive_time(1000)


# pyfftw.interfaces.numpy_fft.fft2(a)
def f(x):
    return x * x


# if __name__ == '__main__':
#     with Pool(5) as p:
#         print(p.map(f, [1, 2, 3]))


kernel_mem = {}

# def tok_deconvolve( tok   ):
#     tok, kernel_fft = tok
#     tok_fft = np.fft.fftn(tok) / kernel_fft
#     return pyfftw.interfaces.numpy_fft.ifftn(tok_fft).real


def cover(ima, h, safe):
    res = np.zeros_like(ima)
    sz, sx = res.shape
    starts_z = np.arange(0, sz, h)
    ends_z = np.minimum(sz, starts_z + h)
    starts_x = np.arange(0, sx, h)
    ends_x = np.minimum(sx, starts_x + h)

    min_mat = np.zeros([len(starts_z), len(starts_x)], "f")
    contributions = np.zeros([7, 7, h, h])
    X, Z = np.meshgrid(np.arange(h) - h / 2, np.arange(h) - h / 2)
    for iz in range(-3, 3 + 1):
        for ix in range(-3, 3 + 1):
            z2 = (-iz * h + Z) ** 2 / ((h * 0.8) ** 2 * 2.0)
            x2 = (-ix * h + X) ** 2 / ((h * 0.8) ** 2 * 2.0)
            contributions[iz + 3, ix + 3] = np.exp(-z2 - x2)

    for myiz, (myz1, myz2) in enumerate(zip(starts_z, ends_z)):
        for myix, (myx1, myx2) in enumerate(zip(starts_x, ends_x)):
            for iz in range(myiz - 3, myiz + 3 + 1):
                for ix in range(myix - 3, myix + 3 + 1):
                    n = 1
                    ibz2_start = min(max(0, iz - n), len(starts_z) - 1)
                    ibz2_end = min(iz + n + 1, len(starts_z))
                    ibx2_start = min(max(0, ix - n), len(starts_x) - 1)
                    ibx2_end = min(ix + n + 1, len(starts_x))

                    z1, z2 = starts_z[ibz2_start], ends_z[ibz2_end - 1]
                    x1, x2 = starts_x[ibx2_start], ends_x[ibx2_end - 1]
                    spurious = max(0, -(ima[z1:z2, x1:x2] - safe).min())
                    res[myz1:myz2, myx1:myx2] += (
                        spurious * contributions[(iz - myiz) + 3, (ix - myix) + 3][: (myz2 - myz1), : (myx2 - myx1)]
                    )

    return res / 3.6



def deconvolve(data, safe, scale_l, fraction, mask_border=100, stripes_intercepts=[], stripes_shear=None, stripes_light_transmission=1.0):
    if  len(stripes_intercepts)==0:
        return deconvolve_simple(data, safe, scale_l, fraction, mask_border)
    else:
        method_1 = deconvolve_simple(data, safe, scale_l, fraction, mask_border)
        method_2 = deconvolve_stripes(data, safe, scale_l, fraction, mask_border, stripes_intercepts, stripes_shear)
        return (stripes_light_transmission*method_1 + (1-stripes_light_transmission)*method_2)


def deconvolve_stripes(data, safe, scale_l, fraction, mask_border, stripes_intercepts, stripes_shear ):

    stripes_intercepts = np.sort(stripes_intercepts)
    
    print(" Padding")
    pad_len = mask_border
    
    pad_len_bis = 200
    pad_len_tot = pad_len + pad_len_bis

    data = np.pad(data, ((pad_len, pad_len), (pad_len, pad_len)), mode="edge")
    data = np.pad(data, ((pad_len_bis, pad_len_bis), (pad_len_bis, pad_len_bis)), mode="constant")


    data_dim_y, data_dim_x = data.shape


    stripes_intercepts = np.array(stripes_intercepts) - stripes_shear * pad_len_tot + pad_len_tot


    mask_left = [0]


    xx,yy = np.meshgrid(np.arange(data_dim_x), np.arange(data_dim_y))
    for intrcpt in stripes_intercepts:
        mask = np.less(  xx - ( intrcpt + yy * stripes_shear ), 0 ) 
        mask_left.append(mask)
        
    mask_left.append(1)

    
    regions_mask = [ ]
    for i in range(0, len(mask_left)-1):
        regions_mask.append(  (1-mask_left[i])*mask_left[i+1]  )


    stripe_gap =  int(4 * scale_l)
    n_stripes = len(stripes_intercepts)
    data_striped_dim_y,  data_striped_dim_x = data_dim_y , data_dim_x + n_stripes * stripe_gap

    data_striped = np.zeros([ data_striped_dim_y,  data_striped_dim_x],"f"    )
    
    regions_mask_striped = [  np.zeros( [data_striped_dim_y,  data_striped_dim_x ] ,"f" )  for m in regions_mask]

    for i, ( m, ms  ) in enumerate(zip(  regions_mask, regions_mask_striped)):
        slice_x = slice(i * stripe_gap, data_striped_dim_x - (n_stripes - i )*stripe_gap)
        ms[ :,  slice_x] = m
        
        data_striped[:, slice_x] = data_striped[:, slice_x] +  m * data

    mask_striped = np.array(regions_mask_striped).sum(axis=0)

    mask_striped [ :pad_len_bis]=0
    mask_striped [-pad_len_bis:]=0
    mask_striped [:, :pad_len_bis]=0
    mask_striped [:, -pad_len_bis:]=0
    
    dims = data_striped.shape[:]    
    if dims in kernel_mem:        
        kernel_fft = kernel_mem[dims]
    else:
        kernel = np.zeros(dims, "f")
        coords_z, coords_x = np.indices(dims)
        distance = np.full(dims, fill_value=np.sum(dims), dtype="f")
        for offset_z in [0, dims[0]]:
            for offset_x in [0, dims[1]]:
                distance = np.minimum(
                    distance,
                    np.sqrt(
                        (coords_z - offset_z) * (coords_z - offset_z) + (coords_x - offset_x) * (coords_x - offset_x)
                    ),
                )

        coda = np.exp(-distance / scale_l) / (distance + 0.00001)
        coda[0, 0] = 0
        coda = fraction * coda / coda.sum()
        kernel[:] = coda
        kernel[0, 0] = 1

        kernel_fft = np.fft.fftn(kernel)

        kernel_mem[dims] = kernel_fft



    data_striped_deconvoluted = data_striped
    
    for i in range(3):
        data_fft = pyfftw.interfaces.numpy_fft.fftn(data_striped_deconvoluted, axes=[0, 1]) * kernel_fft
        data_striped_convoluted = pyfftw.interfaces.numpy_fft.ifftn(data_fft, axes=[0, 1]).real
        data_striped_fake = data_striped_convoluted * ( 1 - mask_striped) +  data_striped * mask_striped
        
        data_fft = pyfftw.interfaces.numpy_fft.fftn(data_striped_fake, axes=[0, 1]) / kernel_fft
        data_striped_deconvoluted = pyfftw.interfaces.numpy_fft.ifftn(data_fft, axes=[0, 1]).real * mask_striped

    data[:]=0    
    for i, ( m, ms  ) in enumerate(zip(  regions_mask, regions_mask_striped)):
        slice_x = slice(i * stripe_gap, data_striped_dim_x - (n_stripes - i )*stripe_gap)
        data = data + m * data_striped_deconvoluted[:, slice_x]
    
    print(" return ")
    res = data[pad_len_tot:-pad_len_tot, pad_len_tot:-pad_len_tot]

    # res[:] = res - fraction

    res = np.maximum(safe, res)
    return res

    # add = cover(res, 100 , safe)

    # return res+add

def deconvolve_simple(data, safe, scale_l, fraction, mask_border=100):
    print(" Padding")
    pad_len = int(round(mask_border))

    pad_len_bis = 512
    pad_len_tot = int(round(pad_len + pad_len_bis))

    data = np.pad(data, ((pad_len, pad_len), (pad_len, pad_len)), mode="edge")
    data = np.pad(data, ((pad_len_bis, pad_len_bis), (pad_len_bis, pad_len_bis)), mode="constant")

    dims = data.shape[:]
    if dims in kernel_mem:
        kernel_fft = kernel_mem[dims]
    else:
        kernel = np.zeros(dims, "f")

        coords_z, coords_x = np.indices(dims)
        distance = np.full(dims, fill_value=np.sum(dims), dtype="f")
        for offset_z in [0, dims[0]]:
            for offset_x in [0, dims[1]]:
                distance = np.minimum(
                    distance,
                    np.sqrt(
                        (coords_z - offset_z) * (coords_z - offset_z) + (coords_x - offset_x) * (coords_x - offset_x)
                    ),
                )

        coda = np.exp(-distance / scale_l) / (distance + 0.00001)
        coda[0, 0] = 0
        coda = fraction * coda / coda.sum()
        kernel[:] = coda
        kernel[0, 0] = 1

        kernel_fft = np.fft.fftn(kernel)

        kernel_mem[dims] = kernel_fft

    # with Pool(32) as p:
    #     done = p.map(tok_deconvolve, todo)

    # for i,tok in enumerate(done):
    #     data[i] = tok

    data_fft = pyfftw.interfaces.numpy_fft.fftn(data, axes=[0, 1]) / kernel_fft
    data[:] = pyfftw.interfaces.numpy_fft.ifftn(data_fft, axes=[0, 1])

    print(" return ")
    res = data[pad_len_tot:-pad_len_tot, pad_len_tot:-pad_len_tot]

    # res[:] = res - fraction

    res = np.maximum(safe, res)
    return res

    # add = cover(res, 100 , safe)

    # return res+add


def test():
    from matplotlib import pyplot as plt
    dims = 100, 1000

    kernel = np.zeros(dims, "f")

    coords_z, coords_x = np.indices(dims)

    distance = np.full(dims, fill_value=np.sum(dims), dtype="f")

    for offset_z in [0, dims[0]]:
        for offset_x in [0, dims[1]]:
            distance = np.minimum(
                distance,
                np.sqrt((coords_z - offset_z) * (coords_z - offset_z) + (coords_x - offset_x) * (coords_x - offset_x)),
            )

    fraction = 2.0
    scala = 3.0
    coda = np.exp(-distance / scala) / (distance + 0.1)
    coda = fraction * coda / coda.sum()

    kernel[:] = coda
    kernel[0, 0] = 1

    kernel_fft = np.fft.fft(kernel)

    data = kernel

    data_fft = np.fft.fft(data) / kernel_fft

    plt.imshow(np.log(data + 0.00000001))
    plt.show()

    data = np.fft.ifft(data_fft).real

    print(data[:10, :10])
