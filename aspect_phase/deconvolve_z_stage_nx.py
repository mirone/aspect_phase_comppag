import sys
import os

# for the interface
import argparse
import json

# numpy eco system
import scipy
import numpy as np
import scipy.ndimage


# for reading the data
from nabu.resources.nxflatfield import update_dataset_info_flats_darks
from nabu.resources.dataset_analyzer import HDF5DatasetAnalyzer




# to manage soft exit of the program when, running on a batch scheduler in best-effort mode, the stop signal is received
import signal

# to write results into a hdf5 file
from filelock import FileLock
import h5py


## The SLD algorithm
from .deconvolve import deconvolve

#####################################
## This unbuffered trick is a technicality
## which force the output from python to be flushed
## otherwise the message from python are not visible
## tuning on slurm and one only sees the output from C
##  https://mail.python.org/pipermail/tutor/2003-November/026645.html
class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream

    def write(self, data):
        self.stream.write(data)
        self.stream.flush()

    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()

    def __getattr__(self, attr):
        return getattr(self.stream, attr)
sys.stdout = Unbuffered(sys.stdout)
########################################




def str2bool(v):
    """ This is a function used in the arg parse to accept various ways to say yes or no
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")

class DictToObj(object):
    """ A class to access dictionaries as objects
    """
    def __init__(self, dictio):
        self.__dict__ = dictio
        
interrupted = False
# where interrupted is set to True by the signal handler
# the code stops after writing the last results or before

def signal_handler(signum, frame):
    global interrupted
    interrupted = True

signal.signal(signal.SIGTERM, signal_handler)


def strip_extension(filename):
    if filename.endswith(".nx"):
        return filename[:-3]
    if filename.endswith(".h5"):
        return filename[:-3]
    if filename.endswith(".hdf5"):
        return filename[:-5]
    return filename


def get_dark_or_flat_current(args, what="flat"):
    """ Get the dark image or the (flat, current) tuple. If the flat is asked
    If iz_start and iz_end have been given on the command line, the returned images
    are clipped to the requested interval (on vertical dimension of the images)
    """
    slice_z = slice(None, None)
    slice_x = slice(None, None)
    
    assert what in ["flats", "darks"]
    file_name = strip_extension(args.original_file) + f"_{what}.hdf5"
    if not os.path.exists(file_name):
        dataset_info = HDF5DatasetAnalyzer( os.path.abspath(args.original_file), extra_options={"h5_entry": args.entry_name})
        update_dataset_info_flats_darks(dataset_info, flatfield_mode=1)
        

    if not os.path.exists(file_name):
        message = f""" Searching {file_name} Error: file not found. reduce flats darks must be already present. """
        raise ValueError(message)

    with h5py.File(file_name, "r") as f:
        g = f[f"/{args.entry_name}/{what}"]
        image = None
        for key in g:
            if str(key).isnumeric():
                image = g[key][()]
                break
    if image is None:
        message = (
            f""" Problem searching a {what} in file {file_name} in  datagroup /{args.entry_name}/{what}. None found"""
        )
        raise RuntimeError(message)

    if what == "darks":
        return image[slice_z, slice_x]

    with h5py.File(file_name, "r") as f:
        current = f[f"/{args.entry_name}/{what}/machine_electric_current"][0]
    return image[slice_z, slice_x], current


def get_radio(args, ipro_h5):
    """ Get the radiography at position ipro_h5 of the hdf5 stack.
    (The position ipro_h5 can be obtained by ipro_map[ipro] where ipro is the map
    which skips flats and darks)
    """
    slice_z = slice(None, None)
    slice_x = slice(None, None)
    with h5py.File(args.original_file, "r") as f:
        raw = f[f"/{args.entry_name}/instrument/detector/data"][ipro_h5]
        d = raw[ slice_z, slice_x]
        # with args.file_lock:
            
        #     with h5py.File(args.target_file, "r+") as f:
        #         f[f"/{args.entry_name}/instrument/detector/data"][ipro_h5] = raw
        return d


def write_result(args, guess, errors, ipro_h5, no_slice = False):
    """ Safe concurrent writing onto a hdf5 file
    """
    if no_slice:
        slice_z = slice(None, None)
        slice_x = slice(None, None)
    else:
        slice_z = slice(None, None)
        slice_x = slice(None, None)
    with args.file_lock:
        with h5py.File(args.target_file, "r+") as f:
            f[f"/{args.entry_name}/instrument/detector/data"][ipro_h5, slice_z, slice_x] = guess
            f[f"/{args.entry_name}/instrument/detector/retrieval_errors"][ipro_h5, : len(errors)] = errors




def get_arguments():
    """
    This is the command line interface
    """
    parser = argparse.ArgumentParser(description="foo")
    parser.add_argument(
        "--original_file",
        required=True,
        help="The nexus file that we are going to duplicate with another file where entryxxxx/instrument/detector/data is replace by a real storage float32",
    )
    parser.add_argument("--target_file", required=True, help="The new nexus filename that we are going to create ")
    parser.add_argument("--entry_name", required=False, help="entry_name", default="entry0000")

    parser.add_argument(
        "--n_chunks",
        required=True,
        help="the projection range is subdivided into n_chunks and a given process will treat only one of these : the number chunk_id ",
        type=int,
    )
    parser.add_argument(
        "--chunk_id",
        required=True,
        help="the projections chunk that we are going to process. An integer from 0 to n_chunks-1",
        type=int,
    )

    parser.add_argument(
        "--diffuse_scale_l", required=False, help="Scale length of the diffused light dumping. ", default=80, type=float
    )
    parser.add_argument(
        "--diffuse_fraction",
        required=False,
        help="Diffused fraction Default 0.0 ( no deconvolution)",
        default=0.0,
        type=float,
    )
    parser.add_argument(
        "--diffuse_mask_border",
        required=False,
        help="The illuminated border around the detector roi. Default is 100",
        default=100,
        type=int,
    )

    parser.add_argument(
        "--stripes_file", required=False, help="The files generated by the map creation tool of nabu-helical which possibly contains gluing stripes informations abut the scintillator"
    )

    parser.add_argument(
        "--stripes_light_transmission",
        required=False,
        default=0.44,
        type=float,
        help="The diffused light transmission through the stripes. A float between 0 and 1"
    )
    
    parser.add_argument(
        "--projection_num_start",
        required=False,
        type=int,
        help="if given, the range of filtered projection will be limited. Useful to do test on a particular slice, by filtering only the needed projections"
    )

    parser.add_argument(
        "--projection_num_end",
        required=False,
        type=int,
        help="if given, the range of filtered projection will be limited. Useful to do test on a particular slice, by filtering only the needed projections"
    )

    parser.add_argument(
        "--skip_existing",
        required=False,
        help='if set to one (1) then skip those radio for which if target result file already exists. Default 1 (It skips). Set it to zero to overwrite previous results. Good for "low" queue',
    )

    args = parser.parse_args()

    args.stripes_intercepts = []
    args.stripes_shear = 0

    if args.stripes_file is not None:
        with h5py.File(args.stripes_file, "r") as f:
            args.stripes_intercepts = f["stripes_intercepts"][()]
            args.stripes_shear = f["stripes_shear"][()]
            
    return args




def already_done(args, map_ipro_h5, ipro, nsteps):
    """
    Check if a radiography has already been processed or not.
    Returns False if the loss function has not yet been set (still zero) for i_pro,
    or if it loss function is abnormally high compared to neighbouring radiographies.
    This function is useful when an interrupted job is restarted after termination
    in best effort mode or when some problematic radiographies need to be processed again
    """
    res = True
    my_col = nsteps - 1
    with args.file_lock:
        with h5py.File(args.target_file, "r") as f:
            values = []
            values2 = []
            for i in range(ipro - 50, ipro + 50):
                if i in map_ipro_h5:
                    values.append(f[f"/{args.entry_name}/instrument/detector/retrieval_errors"][map_ipro_h5[i], my_col])

            d = f[f"/{args.entry_name}/instrument/detector/retrieval_errors"][map_ipro_h5[ipro]]

            if d[my_col] == 0:
                res = False
                return res

            # if d[my_col] > 2 * scipy.median(values):
            #     res = False
            #     return res

    return res


def main():

    
    args = get_arguments()
    args.file_lock = FileLock(args.target_file + "_lock")
    with h5py.File(args.original_file, "r") as f:
        if "image_key_control" in f[f"/{args.entry_name}/instrument/detector/"]:
            control_keys = f[f"/{args.entry_name}/instrument/detector/image_key_control"][()]
        else:
            control_keys = f[f"/{args.entry_name}/instrument/detector/image_key"][()]
        current_h5 = f[f"/{args.entry_name}/control/data"][()]

    # The radiographies with contro key equal to zero are the good ones.
    # NP is the total numer of radiographies that can be found in the stack
    NP = np.equal(control_keys, 0).sum()

    ###############################################################
    # for a projection number ipro in [0, NP[
    # the map ipro_map give ipro_map[ipro]
    # which is he index in the hdf5 stack at which the ipro_th projection can be found skipping non projection frames ( darks, flats, invalid)
    ipro_map = {}
    count = 0
    for i, k in enumerate(control_keys):
        if k == 0:
            ipro_map[count] = i
            count += 1

    my_interval = np.array_split(list(range(NP))[args.projection_num_start:args.projection_num_end], args.n_chunks)[args.chunk_id]
    # the total projections set is split in n_chunks and the present instance of the code
    # will process only one of them: the chunk_id'th.
    # This is how parallel processing is done.

    dark = get_dark_or_flat_current(args, "darks")
    dark = dark.astype("f")
    print(" DARK mean", dark.mean())
    print(" DARK std", dark.std())

    flat0, current_flat = get_dark_or_flat_current(args, "flats")

    if flat0.min() == 0.0 and flat0.max() == 0:
        flat0[:] = 1
        dark[:] = 0
    flat0 = flat0.astype("f")
    flat0[:] -= dark

    if args.diffuse_fraction:
        # the SLD algorithm
        flat0[:] = deconvolve(
            flat0,
            0.05 * dark.mean(),
            args.diffuse_scale_l,
            args.diffuse_fraction,
            args.diffuse_mask_border,
            args.stripes_intercepts,
            args.stripes_shear,
            args.stripes_light_transmission
        )

    flat0[:] *= 200.0 / current_flat
    # data brough up ( or down) to a hypothetical 200mA current

    for ipro in my_interval:
        if interrupted:
            # slurm is finishing, let us do a gentle shutdwon
            sys.exit(99)

        if int(args.skip_existing):
            if already_done(args, ipro_map, ipro, 1):
                continue

        ipro_h5 = ipro_map[ipro]

        ima = get_radio(args, ipro_h5).astype("f")

        ima[:] = ima - dark

        print("before deconvolution: radio mean, min,  max", ima.mean(), ima.min(), ima.max())


        print(" IMA mean", ima.mean())
        print(" IMA std", ima.std())


        
        if args.diffuse_fraction:
            ima[:] = deconvolve(
                ima, 0.05 * dark.mean(),
                args.diffuse_scale_l,
                args.diffuse_fraction,
                args.diffuse_mask_border,
                args.stripes_intercepts,
                args.stripes_shear,
                args.stripes_light_transmission
            )

        print("dopo  IMA mean", ima.mean())
        print("dopo  IMA std", ima.std())

            
        print(" after deconvolution: radio mean, min,  max", ima.mean(), ima.min(), ima.max())

        # everything brought up to the level of an hypothetical 200 mA strong beam
        factor_current = 200.0 / current_h5[ipro_h5]

        data = (ima * factor_current).astype("f")

        myff = np.zeros_like(data)
        myff[:] = flat0

        data[:] = data/ myff
        
        retrieved_transmission = data
        print(" ERROR  ", ipro, ipro_h5, 1.0)

        write_result(args, retrieved_transmission, [1.0], ipro_h5)
