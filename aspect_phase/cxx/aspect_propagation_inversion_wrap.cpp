 
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <tuple>

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

#define FORCE_IMPORT_ARRAY

#include<string>
#include <pybind11/numpy.h>
#include<exception>
#include"utilities.h"

double  aspect_CG(
		  int size0,
		  int size1,
		  float *data,
		  float *transm,
		  float DBR, // delta over beta, interally it will be divided by two (delta over absorption)
		  float SOURCE_Y, 
		  float SOURCE_X, 
		  float SOURCE_DISTANCE, // in meters
		  float DETECTOR_DISTANCE, // in meters
		  float IMAGE_PIXEL_SIZE,  // in micron, at the detector, not the voxel
		  float E_kev,
		  int NitersCG ,
		  float step_factor,
		  int reduction_factor,
		  int verbose,
		  float *rmd,
		  int n_spettro,
		  float *spettro_data,
		  int ovs,
		  int aggregate_period,
		  int salva_input,
		  int just_forward,
		  int do_pag
		  ) ;


void gpu_med( int ny, int nx, float *data, float *result, int hwy, int hwx, float threshold) ;


using namespace pybind11::literals;  // to bring in the `_a` literal, they are used to pass arguments by keyword


// ----------------
// Python interface
// ----------------

namespace py = pybind11;

PYBIND11_MODULE(aspect_phase_retrieval,m)
{
  // xt::import_numpy();
  m.doc() = "nabu c/c++ acceleration for aspect_phase_retrieval";

  m.def("gpu_med", []( py::array_t<float> &input,
		       py::array_t<float> &output, 
		       int w_y,
		       int w_x
		       )
		   {
		    
		    py::buffer_info input_info  =  nabuxx::checked_buffer_request(input , 'f' , "filter of aspect_phase_retrieval" , "input",  nabuxx::memlayouts::c_contiguous, 2 );
		    py::buffer_info output_info  =  nabuxx::checked_buffer_request(output , 'f' , "filter of aspect_phase_retrieval" , "output",  nabuxx::memlayouts::c_contiguous, 2 );
		    
		    if(  input_info.shape[0] != output_info.shape[0] || input_info.shape[1] != output_info.shape[1]   ) {
		      std::stringstream ss;
		      ss << "The argumens input and output,  to filter,  must have  the same shape but their shaper are " <<  input_info.shape[0] << "," <<  input_info.shape[1]  ;
		      ss << " and " <<  output_info.shape[0] << "," <<  output_info.shape[1]  ;
		      throw std::invalid_argument(ss.str() );
		    }
		    
		    
		    gpu_med(
			    input_info.shape[0],
			    input_info.shape[1],
			    (float*) input_info.ptr, 
			    (float*) output_info.ptr,
			    (w_y-1)/2,
			    (w_x-1)/2,	    
			    -1.0e30
			    ) ;
		    return py::none();
		   },
	py::arg("data"),
	py::arg("output"),
	py::arg("w_y")=3 ,
	py::arg("w_x")=3 
	);
  

   
  m.def("filter", []( py::array_t<float> &data,
		      py::array_t<float> &solution, 
		      float DBR,
		      float SOURCE_Y,
		      float SOURCE_X,
		      float SOURCE_DISTANCE,
		      float DETECTOR_DISTANCE,
		      float IMAGE_PIXEL_SIZE,
		      float E_kev,
		      int NitersCG,
		      float step_factor,
		      int reduction_factor,
		      int verbose,
		      py::array_t<float> &myff,
		      py::array_t<float> &spettro_data,
		      int ovs,
		      int aggregate_period,
		      int salva_input,
		      int just_forward,
		      int do_pag
		      )
		  {
		    
		    py::buffer_info data_info  =  nabuxx::checked_buffer_request(data , 'f' , "filter of aspect_phase_retrieval" , "data",  nabuxx::memlayouts::c_contiguous, 2 );
		    py::buffer_info solution_info  =  nabuxx::checked_buffer_request(solution , 'f' , "filter of aspect_phase_retrieval" , "solution",  nabuxx::memlayouts::c_contiguous, 2 );
		    
		    if(  data_info.shape[0] != solution_info.shape[0] || data_info.shape[1] != solution_info.shape[1]   ) {
		      std::stringstream ss;
		      ss << "The argumens data and solution,  to filter,  must have  the same shape but their shaper are " <<  data_info.shape[0] << "," <<  data_info.shape[1]  ;
		      ss << " and " <<  solution_info.shape[0] << "," <<  solution_info.shape[1]  ;
		      throw std::invalid_argument(ss.str() );
		    }
		    
		    py::buffer_info  myff_info = myff.request();
		    float *myff_ptr;
		    if (myff_info.ndim != -1) {
		      myff_info  =  nabuxx::checked_buffer_request(myff , 'f' , "filter of aspect_phase_retrieval" , "myff",  nabuxx::memlayouts::c_contiguous, 2 );
		      if(  data_info.shape[0] != myff_info.shape[0] || data_info.shape[1] != myff_info.shape[1]   ) {
			std::stringstream ss;
			ss << "The argumens data and myff,  to filter,  must have  the same shape but their shaper are " <<  data_info.shape[0] << "," <<  data_info.shape[1]  ;
			ss << " and " <<  myff_info.shape[0] << "," <<  myff_info.shape[1]  ;
			throw std::invalid_argument(ss.str() );
		      }
		      myff_ptr = (float*) myff_info.ptr ;
		    } else {
		      myff_ptr=NULL;
		    }
		    
		    py::buffer_info  spettro_data_info = spettro_data.request();
		    float *spettro_data_ptr;
		    spettro_data_info  =  nabuxx::checked_buffer_request(spettro_data , 'f' , "filter of aspect_phase_retrieval" , "spettro_data",  nabuxx::memlayouts::c_contiguous, 2 );
		    if(    spettro_data_info.shape[1] != 4   ) {
		      std::stringstream ss;
		      ss << "The argumens spettro_data must have dimension n_spettro x 4  where each quadruplet is formed by  " ;
		      ss << " ene_ratio beta_ratio delta_ratio fraction. But dimensions were " <<  spettro_data_info.shape[0] << "," <<  spettro_data_info.shape[1]  ;
		      throw std::invalid_argument(ss.str() );
		    }
		    spettro_data_ptr = (float*) spettro_data_info.ptr ;
		    int n_spettro = spettro_data_info.shape[0] ; 
		    
		    return aspect_CG(
				     data_info.shape[0],
				     data_info.shape[1],
				     (float*) data_info.ptr, 
				     (float*) solution_info.ptr, 
				     DBR,
				     SOURCE_Y,
				     SOURCE_X,
				     SOURCE_DISTANCE,
				     DETECTOR_DISTANCE,
				     IMAGE_PIXEL_SIZE,
				     E_kev,
				     NitersCG,
				     step_factor,
				     reduction_factor,
				     verbose,
				     myff_ptr,
				     n_spettro,
				     spettro_data_ptr,
				     ovs,
				     aggregate_period,
				     salva_input,
				     just_forward,
				     do_pag
				     ) ; 
		  },
	py::arg("data"),
	py::arg("solution"),
	py::arg("delta_beta_ratio") ,
	py::arg("source_y_px"),
	py::arg("source_x_px"),
	py::arg("source_distance_meters") ,
	py::arg("detector_distance_meters") ,
	py::arg("detector_pixel_size_micron") ,
	py::arg("e_kev") ,
	py::arg("niters_max")=400 ,
	py::arg("step_factor")=0.001,
	py::arg("reduction_factor")=1,
	py::arg("verbose")=0,
	py::arg("myff")=py::none(),
	py::arg("spettro_data")=py::none(),
	py::arg("ovs")=1,
	py::arg("aggregate_period")=100,
	py::arg("salva_input")=0,
	py::arg("just_forward")=0,
	py::arg("do_pag")=0
	);
     
}

