#define signf_copy(a,b) copysignf(b,a )

static int iDivUp(int a, int b){
  return (a % b != 0) ? (a / b + 1) : (a / b);
}
#define AUXILIARY_EXPANSION 1

#define clamp(x,a,b) ( fmaxf( (a), fminf( (x), (b) ) ) )

#define  RESCALE_FORWARD  1
#define  RESCALE_BACKWARD -1
#define REPLICATE_FOR_ABS 1



#  define CUDACHECK \
  { cudaDeviceSynchronize(); \
    cudaError_t last = cudaGetLastError();\
    if(last!=cudaSuccess) {\
      printf("ERRORX: %s  %s  %i \n", cudaGetErrorString( last),    __FILE__, __LINE__    );    \
      exit(1);\
    }\
  }

#  define CUDA_SAFE_CALL_NO_SYNC( call) {				\
    cudaError err = call;						\
    if( cudaSuccess != err) {						\
      fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",	\
	      __FILE__, __LINE__, cudaGetErrorString( err) );		\
      exit(EXIT_FAILURE);						\
    } }
 
#  define CUDA_SAFE_CALL( call)     CUDA_SAFE_CALL_NO_SYNC(call);	\


