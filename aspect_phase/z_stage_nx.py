import sys
import os

# for the interface
import argparse
import json

# numpy eco system
import scipy
import numpy as np
import scipy.ndimage

# for GPU initialisation
import pycuda.driver as cuda
import pycuda.autoinit


# for reading the data
from nabu.resources.nxflatfield import update_dataset_info_flats_darks
from nabu.resources.dataset_analyzer import HDF5DatasetAnalyzer


# The EPR algorithm
import aspect_phase.aspect_phase_retrieval as spct


## these modules are used to detect, after application of EPR,
## pathological spots where the gradient takes zig-zag shape taking altenatively a large positive value and
## a large negative value going from one pixel to the neighbouring one.
## These regions are detected according to a threshold, in pixel units, which is compared to the shifts.
## Then a local low pass filter is applied to mend the solution, and EPR is rerunned using the mended solution as guess 
from scipy.ndimage import laplace, convolve, gaussian_filter, binary_dilation
from aspect_phase.penicilline import filter_zigzag


# to manage soft exit of the program when, running on a batch scheduler in best-effort mode, the stop signal is received
import signal

# to write results into a hdf5 file
from filelock import FileLock
import h5py


## The SLD algorithm
from .deconvolve import deconvolve

#####################################
## This unbuffered trick is a technicality
## which force the output from python to be flushed
## otherwise the message from python are not visible
## tuning on slurm and one only sees the output from C
##  https://mail.python.org/pipermail/tutor/2003-November/026645.html
class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream

    def write(self, data):
        self.stream.write(data)
        self.stream.flush()

    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()

    def __getattr__(self, attr):
        return getattr(self.stream, attr)
sys.stdout = Unbuffered(sys.stdout)
########################################




def str2bool(v):
    """ This is a function used in the arg parse to accept various ways to say yes or no
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


class DictToObj(object):
    """ A class to access dictionaries as objects
    """
    def __init__(self, dictio):
        self.__dict__ = dictio



        
interrupted = False
# where interrupted is set to True by the signal handler
# the code stops after writing the last results or before

def signal_handler(signum, frame):
    global interrupted
    interrupted = True

signal.signal(signal.SIGTERM, signal_handler)


cuda.Context.synchronize()


def strip_extension(filename):
    if filename.endswith(".nx"):
        return filename[:-3]
    if filename.endswith(".h5"):
        return filename[:-3]
    if filename.endswith(".hdf5"):
        return filename[:-5]
    return filename


def get_dark_or_flat_current(args, what="flat"):
    """ Get the dark image or the (flat, current) tuple. If the flat is asked
    If iz_start and iz_end have been given on the command line, the returned images
    are clipped to the requested interval (on vertical dimension of the images)
    """
    slice_z = slice(args.iz_start, args.iz_end)
    slice_x = slice(args.ix_start, args.ix_end)
    
    assert what in ["flats", "darks"]
    file_name = strip_extension(args.original_file) + f"_{what}.hdf5"
    if not os.path.exists(file_name):
        dataset_info = HDF5DatasetAnalyzer( os.path.abspath(args.original_file), extra_options={"h5_entry": args.entry_name})
        update_dataset_info_flats_darks(dataset_info, flatfield_mode=1)
        

    if not os.path.exists(file_name):
        message = f""" Searching {file_name} Error: file not found. reduce flats darks must be already present. """
        raise ValueError(message)

    with h5py.File(file_name, "r") as f:
        g = f[f"/{args.entry_name}/{what}"]
        image = None
        for key in g:
            if str(key).isnumeric():
                image = g[key][()]
                break
    if image is None:
        message = (
            f""" Problem searching a {what} in file {file_name} in  datagroup /{args.entry_name}/{what}. None found"""
        )
        raise RuntimeError(message)

    if what == "darks":
        return image[slice_z, slice_x]

    with h5py.File(file_name, "r") as f:
        current = f[f"/{args.entry_name}/{what}/machine_electric_current"][0]
    return image[slice_z, slice_x], current


def get_radio(args, ipro_h5):
    """ Get the radiography at position ipro_h5 of the hdf5 stack.
    (The position ipro_h5 can be obtained by ipro_map[ipro] where ipro is the map
    which skips flats and darks)
    """
    slice_z = slice(args.iz_start, args.iz_end)
    slice_x = slice(args.ix_start, args.ix_end)
    with h5py.File(args.original_file, "r") as f:
        raw = f[f"/{args.entry_name}/instrument/detector/data"][ipro_h5]
        d = raw[ slice_z, slice_x]
        with args.file_lock:
            with h5py.File(args.target_file, "r+") as f:
                f[f"/{args.entry_name}/instrument/detector/data"][ipro_h5] = raw
        return d


def write_result(args, guess, errors, ipro_h5, no_slice = False):
    """ Safe concurrent writing onto a hdf5 file
    """
    if no_slice:
        slice_z = slice(None, None)
        slice_x = slice(None, None)
    else:
        slice_z = slice(args.iz_start, args.iz_end)
        slice_x = slice(args.ix_start, args.ix_end)
    with args.file_lock:
        with h5py.File(args.target_file, "r+") as f:
            f[f"/{args.entry_name}/instrument/detector/data"][ipro_h5, slice_z, slice_x] = guess
            f[f"/{args.entry_name}/instrument/detector/retrieval_errors"][ipro_h5, : len(errors)] = errors



def already_done(args, map_ipro_h5, ipro, nsteps):
    """
    Check if a radiography has already been processed or not.
    Returns False if the loss function has not yet been set (still zero) for i_pro,
    or if it loss function is abnormally high compared to neighbouring radiographies.
    This function is useful when an interrupted job is restarted after termination
    in best effort mode or when some problematic radiographies need to be processed again
    """
    res = True
    my_col = nsteps - 1
    with args.file_lock:
        with h5py.File(args.target_file, "r") as f:
            values = []
            values2 = []
            for i in range(ipro - 50, ipro + 50):
                if i in map_ipro_h5:
                    values.append(f[f"/{args.entry_name}/instrument/detector/retrieval_errors"][map_ipro_h5[i], my_col])

            d = f[f"/{args.entry_name}/instrument/detector/retrieval_errors"][map_ipro_h5[ipro]]

            if d[my_col] == 0:
                res = False
                return res

            if d[my_col] > 2 * scipy.median(values):
                res = False
                return res

    return res


def get_arguments():
    """
    This is the command line interface
    """
    parser = argparse.ArgumentParser(description="foo")
    parser.add_argument(
        "--original_file",
        required=True,
        help="The nexus file that we are going to duplicate with another file where entryxxxx/instrument/detector/data is replace by a real storage float32",
    )
    parser.add_argument("--target_file", required=True, help="The new nexus filename that we are going to create ")
    parser.add_argument("--entry_name", required=False, help="entry_name", default="entry0000")

    parser.add_argument(
        "--n_chunks",
        required=True,
        help="the projection range is subdivided into n_chunks and a given process will treat only one of these : the number chunk_id ",
        type=int,
    )
    parser.add_argument(
        "--chunk_id",
        required=True,
        help="the projections chunk that we are going to process. An integer from 0 to n_chunks-1",
        type=int,
    )

    parser.add_argument("--source_dist_m", required=True, help="The source distance in meter")
    parser.add_argument("--detector_dist_m", required=True, help="The detector distance in meter")
    parser.add_argument(
        "--pixel_size_um",
        required=True,
        help="The pixel size in micron, this is the pixel size at the sample position: the voxel size",
    )

    parser.add_argument(
        "--skip_existing",
        required=False,
        help='if set to one (1) then skip those radio for which if target result file already exists. Default 1 (It skips). Set it to zero to overwrite previous results. Good for "low" queue',
    )
    parser.add_argument(
        "--spectra_data_file",
        required=True,
        help="contains a list of lines with 4 elements each : ene_ratio b eta_ratio delta_ratio fraction. The Nominal energy(Kev) must be on the first line of the header, after =. The nominal delta-beta ratio is in the second line of the header",
    )

    parser.add_argument("--iz_start", required=False, help="z start. Optional", default=None, type=int)
    parser.add_argument("--iz_end", required=False, help="z end. Optional", default=None, type=int)
    parser.add_argument("--ix_start", required=False, help="z start. Optional", default=None, type=int)
    parser.add_argument("--ix_end", required=False, help="z end. Optional", default=None, type=int)

    parser.add_argument(
        "--pathological_threshold",
        required=False,
        help="Threshold applied to detect pathological points. Default is 0.0(no filtering)",
        default=0.0,
        type=float,
    )
    parser.add_argument(
        "--diffuse_scale_l", required=False, help="Scale length of the diffused light dumping. ", default=80, type=float
    )
    parser.add_argument(
        "--diffuse_fraction",
        required=False,
        help="Diffused fraction Default 0.0 ( no deconvolution)",
        default=0.0,
        type=float,
    )
    parser.add_argument(
        "--diffuse_mask_border",
        required=False,
        help="The illuminated border around the detector roi. Default is 100",
        default=100,
        type=int,
    )

    parser.add_argument(
        "--sequence_file", required=True, help="The sequence file, a default standard one is provided by night_rail"
    )
    
    parser.add_argument(
        "--stripes_file", required=False, help="The files generated by the map creation tool of nabu-helical which possibly contains gluing stripes informations abut the scintillator"
    )
    parser.add_argument(
        "--stripes_light_transmission",
        required=False,
        default=0.44,
        type=float,
        help="The diffused light transmission through the stripes. A float between 0 and 1"
    )

    parser.add_argument(
        "--projection_num_start",
        required=False,
        type=int,
        help="if given, the range of filtered projection will be limited. Useful to do test on a particular slice, by filtering only the needed projections"
    )
    parser.add_argument(
        "--projection_num_end",
        required=False,
        type=int,
        help="if given, the range of filtered projection will be limited. Useful to do test on a particular slice, by filtering only the needed projections"
    )

    
    args = parser.parse_args()

    args.spettro_data = np.loadtxt(args.spectra_data_file).astype("f")
    if len(args.spettro_data.shape) == 1:
        args.spettro_data.shape = (1,) + args.spettro_data.shape

    with open(args.spectra_data_file, "r") as f:
        header = f.readline()
        pos = header.rfind("=")
        if pos == -1:
            raise ValueError(f"""Error searching for Energy value in this line of the spectra_data file: {header}""")
        args.energy_kev = float(header[pos + 1 :])

        header = f.readline()
        pos = header.rfind("=")
        if pos == -1:
            raise ValueError(f"""Error searching for DBR value in this line of the spectra_data file: {header}""")
        args.dbr = float(header[pos + 1 :])


    args.stripes_intercepts = []
    args.stripes_shear = 0

    if args.stripes_file is not None:
        with h5py.File(args.stripes_file, "r") as f:
            args.stripes_intercepts = f["stripes_intercepts"][()]
            args.stripes_shear = f["stripes_shear"][()]
            
    return args


def main():

    
    args = get_arguments()
    # parse user choices from the command line

    args.file_lock = FileLock(args.target_file + "_lock")
    # The lock file for the target hdf5  file, for parallel processing.
    
    with h5py.File(args.original_file, "r") as f:
        # read the current and the kind of images that one can find in the stack ( control_keys)
        if "image_key_control" in f[f"/{args.entry_name}/instrument/detector/"]:
            control_keys = f[f"/{args.entry_name}/instrument/detector/image_key_control"][()]
        else:
            control_keys = f[f"/{args.entry_name}/instrument/detector/image_key"][()]
        current_h5 = f[f"/{args.entry_name}/control/data"][()]


    # The radiographies with contro key equal to zero are the good ones.
    # NP is the total numer of radiographies that can be found in the stack
    NP = np.equal(control_keys, 0).sum()

    ###############################################################
    # for a projection number ipro in [0, NP[
    # the map ipro_map give ipro_map[ipro]
    # which is he index in the hdf5 stack at which the ipro_th projection can be found skipping non projection frames ( darks, flats, invalid)
    ipro_map = {}
    count = 0
    for i, k in enumerate(control_keys):
        if k == 0:
            ipro_map[count] = i
            count += 1
    #
    ##############################

    my_interval = np.array_split(list(range(NP))[args.projection_num_start:args.projection_num_end], args.n_chunks)[args.chunk_id]
    # the total projections set is split in n_chunks and the present instance of the code
    # will process only one of them: the chunk_id'th.
    # This is how parallel processing is done.

    detector_dist_m = float(args.detector_dist_m)
    source_dist_m = float(args.source_dist_m)
    pixel_size_um = float(args.pixel_size_um)

    if args.skip_existing is not None:
        skip_existing = int(args.skip_existing)
    else:
        skip_existing = True

    dark = get_dark_or_flat_current(args, "darks")
    dark = dark.astype("f")
    print(" DARK mean", dark.mean())
    print(" DARK mean", dark.std())

    flat0, current_flat = get_dark_or_flat_current(args, "flats")

    if flat0.min() == 0.0 and flat0.max() == 0:
        flat0[:] = 1
        dark[:] = 0
    flat0 = flat0.astype("f")
    flat0[:] -= dark

    if args.diffuse_fraction:
        # the SLD algorithm
        flat0[:] = deconvolve(
            flat0, 0.05 * dark.mean(),
            args.diffuse_scale_l,
            args.diffuse_fraction,
            args.diffuse_mask_border,
            args.stripes_intercepts,
            args.stripes_shear,
            args.stripes_light_transmission,
        )

    flat0[:] *= 200.0 / current_flat
    # data brough up ( or down) to a hypothetical 200mA current

    with open(args.sequence_file, "r") as fp:
        # The sequence of different resolution scales for EPR preconditioning
        sequence_infos = json.load(fp)


    for ipro in my_interval:
        if interrupted:
            # slurm is finishing, let us do a gentle shutdwon
            sys.exit(99)

        if skip_existing:
            if already_done(args, ipro_map, ipro, nsteps=len(sequence_infos)):
                continue

        ipro_h5 = ipro_map[ipro]

        ima = get_radio(args, ipro_h5).astype("f")

        ima[:] = ima - dark

        print("before deconvolution: radio mean, min,  max", ima.mean(), ima.min(), ima.max())

        if args.diffuse_fraction:
            ima[:] = deconvolve(
                ima, 0.05 * dark.mean(),
                args.diffuse_scale_l,
                args.diffuse_fraction,
                args.diffuse_mask_border,
                args.stripes_intercepts,
                args.stripes_shear,
                args.stripes_light_transmission,
            )

        print(" after deconvolution: radio mean, min,  max", ima.mean(), ima.min(), ima.max())

        # everything brought up to the level of an hypothetical 200 mA strong beam
        factor_current = 200.0 / current_h5[ipro_h5]

        data = (ima * factor_current).astype("f")

        myff = np.zeros_like(data)
        myff[:] = flat0

        pad_up = 0
        pad_right = 0

        max_expansion_unit = 2 ** (sequence_infos[0]["expansion"] - 1)

        if data.shape[0] % max_expansion_unit:
            pad_up = max_expansion_unit - data.shape[0] % max_expansion_unit

        if data.shape[1] % max_expansion_unit:
            pad_right = max_expansion_unit - data.shape[1] % max_expansion_unit

        data_padded = np.pad(data, ([0, pad_up], [0, pad_right]), mode="edge")
        myff_padded = np.pad(myff, ([0, pad_up], [0, pad_right]), mode="edge")


        errors = []
        retrieved_transmission = np.ones_like(data_padded)
        retrieved_transmission[:] = data_padded / myff_padded

        # from now on "data" is what we could get with a ff of 1 everywhere
        data_padded[:] = data_padded / myff_padded

        myff_padded[:] = 1

        ## Oversampling = 2,3.. to be tested yet
        ovs = 1

        if detector_dist_m > 0:
          for step_infos_dict in sequence_infos:
            step_infos = DictToObj(step_infos_dict)


            n_spectra = len(args.spettro_data)

            my_data_padded = data_padded
            my_retrieved_transmission = retrieved_transmission

            if interrupted:
                # gentle shut down
                sys.exit(99)

            beam_y = (data_padded.shape[0] - 1) / 2
            beam_x = (data_padded.shape[1] - 1) / 2

            source_dist_m_infinity = 1.0e30
            # We will use the Pogany theorem to rescale wavelenght and pixel size to the parallel geometry case

            for i in range(step_infos.n_repets):
                # Scaling Pogany law to reconduct the a problem to parallel geometry
                effective_energy_kev = args.energy_kev * source_dist_m / (source_dist_m + detector_dist_m)
                effective_pixel_size_um = pixel_size_um * (source_dist_m + detector_dist_m) / source_dist_m

                if step_infos.poly:
                    # each energy point treated separately
                    aggregate_period = 1
                else:
                    # absorption weighted aggregation and propagation with effective delta/beta ratio
                    aggregate_period = n_spectra + 1


                # Running the EPR algorithm
                res = spct.filter(
                    my_data_padded,
                    my_retrieved_transmission,
                    args.dbr,
                    beam_y,
                    beam_x,
                    source_dist_m_infinity,
                    detector_dist_m,
                    effective_pixel_size_um,
                    effective_energy_kev,
                    step_infos.niters,
                    step_infos.deriv_step,
                    step_infos.expansion,
                    0,  # verbose
                    myff_padded,
                    args.spettro_data,
                    ovs,
                    aggregate_period
                )
                
                if args.pathological_threshold:
                    print(" Filtering pathological regions")
                    dist_um = detector_dist_m * 1.0e6
                    lmbd_um = (12398.42 / (effective_energy_kev * 1.0e3)) * 1.0e-4
                    factor = args.dbr * dist_um * lmbd_um / effective_pixel_size_um / effective_pixel_size_um

                    sol = (-np.log(my_retrieved_transmission)).astype("f")
                    # sol.tofile("sol.raw")
                    dl = laplace(sol * factor).astype("f")
                    mask = np.zeros_like(dl)

                    filter_zigzag.filtra_petri_dish(dl, mask, args.pathological_threshold)

                    mask = binary_dilation(mask)
                    mask = binary_dilation(mask)

                    print(" N pathological points ", mask.sum(), res)

                    my_retrieved_transmission[mask > 0] = gaussian_filter(my_retrieved_transmission, 1.0)[mask > 0]

                print(" res ", i, step_infos.expansion, ovs, aggregate_period, res)

            errors.append(res)

        if interrupted:
            sys.exit(99)

        limits = [-pad_up or None, -pad_right or None]

        retrieved_transmission = retrieved_transmission[: limits[0], : limits[1]]
        print(" ERROR  ", ipro, ipro_h5, errors)


        write_result(args, retrieved_transmission, errors, ipro_h5)
