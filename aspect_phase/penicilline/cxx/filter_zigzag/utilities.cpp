#include<stdio.h>
#include<sstream>
#include<iostream>
#include<exception>

#include "utilities.h"



namespace py = pybind11;



  size_t  buffer_size( py::buffer_info &info) {
    size_t prod=1;
    for(int i=0; i<info.ndim; i++) {
      prod = prod * info.shape[i];
    }
    return prod;
  }

  
  bool check_c_contiguity(py::buffer_info &info, int till_dimension ) {
    int ndim = info.ndim;
    size_t stride = 0;
    size_t prod=1;

    
    
    if (till_dimension>=0) till_dimension = ndim-1 - till_dimension ;
    else till_dimension = 0; 
    
    for(int i=ndim-1; i>= till_dimension; i--) {

      stride =  prod * info.itemsize ;

      prod = prod * info.shape[i];
      
      if ( stride != (size_t) info.strides[i] ) {
	return false;
      }
    }
    return true;  
  }
  bool check_f_contiguity(py::buffer_info &info) {
    int ndim = info.ndim;
    size_t stride = 0;
    size_t prod=1;
    for(int i=0; i<ndim; i++) {

      stride = prod * info.itemsize;
      prod = prod * info.shape[i];

      if ( stride != (size_t) info.strides[i] ) {
	return false;
      }
    }
    return true;  
  }

  bool check_f_or_c_contiguity(   py::buffer_info &info  ) {
    return  check_c_contiguity(info)  ||  check_f_contiguity(info)  ; 

  }


  py::buffer_info checked_buffer_request(
					 py::buffer &buffer,
					 char type ,
					 std::string method_name,
					 std::string variable_name,
					 memlayouts layout,
					 int ndim,
					 int till_dimension
					 ) {

    py::buffer_info  my_buffer_info = buffer.request();


    if( ndim!=-1 ) {
      if( my_buffer_info.ndim && my_buffer_info.ndim!=ndim) {
	std::stringstream ss;
	ss << "The argument to " <<  method_name <<  " must have  "  << ndim << " dimensions but it has " << my_buffer_info.ndim  << " dimensions "  ; 
	throw std::invalid_argument(ss.str() );
      }
    }

    // if clause to check if  None has not been  passed as buffer from python
    if( my_buffer_info.ndim !=0  ) {
    
      if( my_buffer_info.format[0] != type) {
	throw std::invalid_argument("The argument to " + method_name +  " must be an array of type " + type);
      }

	
      // check contiguity


      if( layout ==   memlayouts::any ) {
      } else if (   (layout & memlayouts::c_contiguous  ) && (layout & memlayouts::f_contiguous  )  ) {
	if ( ! (check_c_contiguity(my_buffer_info, till_dimension) || check_f_contiguity(my_buffer_info) ) ) {
	  throw std::invalid_argument("argument  " + variable_name + "  to  "+ method_name +"  should be C or F contiguous");
	}
      } else if (   (layout & memlayouts::c_contiguous  )   ) {
	if ( ! check_c_contiguity(my_buffer_info, till_dimension) ) {
	  throw std::invalid_argument("argument  " + variable_name + "  to  "+ method_name +"  should be C contiguous");
	}
      } else if (   (layout & memlayouts::f_contiguous  )   ) {
	if ( ! check_f_contiguity(my_buffer_info) ) {
	  throw std::invalid_argument("argument  " + variable_name + "  to  "+ method_name +"  should be F contiguous");
	}
      } 
    }
   
    return my_buffer_info;
    
  }


  memlayouts operator | (memlayouts a, memlayouts b)
  {
    return (memlayouts )( (unsigned int) a| (unsigned int) b);
  }


