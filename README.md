# EPR. The Eikonal Phase Retrieval

this repository is linked to the paper

## Eikonal phase retrieval: Unleashing the fourth generation sources potential for enhanced propagation based tomography on biological samples.

## Installing and running a demo

The demo consists in experimental data, instruction to install the code, scripts and configuration files to perform a reconstruction from raw data using the EPR algorithm. it can be found here:

[MIRONE, A., BRUNET, J., Boistel, R., Sowinski, M., BERRUYER, C., Payno, H., Boller, E., Paleo, P., Walsh, C. L., Lee, P. D., & TAFFOREAU, P. (2023). Demo for the Eikonal Phase Retrieval Algorithm]( European Synchrotron Radiation Facility.https://doi.org/10.15151/ESRF-DC-1415281265)

